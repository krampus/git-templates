#! /usr/bin/env bash

TEMPLATE_PATH=$(dirname $(realpath -s $0))
TEMPLATE_NAME='python-basic'
PROJECT_PATH=$1

mkgitignore() {
    curl https://raw.githubusercontent.com/github/gitignore/main/Global/Emacs.gitignore
    curl https://raw.githubusercontent.com/github/gitignore/main/Python.gitignore
    echo
    echo "## Project-specific gitignores"
    echo "/sandbox"
    echo "/$PROJECT_SRC_NAME/_version.py"
}

source "$TEMPLATE_PATH/_common.sh"

PYTHON_CMD=$2
if [ -z "$PYTHON_CMD" ]; then
    PYTHON_CMD='/usr/sbin/python3.9'
fi
PYTHON_VER=$($PYTHON_CMD -c "import sys; print(f'{sys.version_info.major}.{sys.version_info.minor}')")
info "Using $PYTHON_CMD (version $PYTHON_VER)"

PROJECT_SRC_NAME=$(echo "$PROJECT_NAME" | sed 's/-/_/g')

mkproject

mv _src "$PROJECT_SRC_NAME"
sed -i -e "s/{{python_version}}/$PYTHON_VER/g" \
    -e "s/{{project_name}}/$PROJECT_NAME/g" \
    -e "s/{{project_src}}/$PROJECT_SRC_NAME/g" \
    -e "s/{{user_name}}/$USER_NAME/g" \
    -e "s/{{user_email}}/$USER_EMAIL/g" \
    {pyproject.toml,*/*.py}

gitinit

info "Initializing python venv in \`venv\`"
$PYTHON_CMD -m venv venv

info "Installing dev requirements in venv"
venv/bin/pip --require-virtualenv install -e .[dev]

info "Initialized new basic python project in $PROJECT_PATH"
