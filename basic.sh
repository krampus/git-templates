#! /usr/bin/env bash

TEMPLATE_PATH=$(dirname $(realpath -s $0))
TEMPLATE_NAME='basic'
PROJECT_PATH=$1

mkgitignore() {
    curl https://raw.githubusercontent.com/github/gitignore/main/Global/Emacs.gitignore
    echo
    echo "## Project-specific gitignores"
    echo "/sandbox"
}

source "$TEMPLATE_PATH/_common.sh"

mkproject
gitinit

info "Initialized new basic project in $PROJECT_PATH"
