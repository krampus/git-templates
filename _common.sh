#! /usr/bin/env bash
# define common elements used by other templates

info() {
    echo -e "\e[1m$1\e[0m"
}

if [ -z "$PROJECT_PATH" ]; then
    info "Missing required argument: [project path]"
    exit 1
fi
PROJECT_NAME=$(basename "$PROJECT_PATH")

CURRENT_YEAR=$(date "+%Y")
USER_NAME=$(git config --get user.name || echo "unknown")
USER_EMAIL=$(git config --get user.email || echo "unknown")

mklicense_mit() {
    sed -e "s/{{year}}/$CURRENT_YEAR/g" \
        -e "s/{{user_name}}/$USER_NAME/g" \
        -e "s/{{user_email}}/$USER_EMAIL/g" \
        "$TEMPLATE_PATH/_license/MIT.license"
}

mkproject() {
    info "Creating project..."
    mkdir -p "$PROJECT_PATH"
    cp -avT "$TEMPLATE_PATH/$TEMPLATE_NAME" "$PROJECT_PATH"
    cd "$PROJECT_PATH"

    info "Writing stub README.md..."
    echo "# $PROJECT_NAME" > README.md

    info "Generating .gitignore..."
    mkgitignore > .gitignore

    info "Generating license (MIT)..."
    mklicense_mit > LICENSE
}

gitinit() {
    info "Initializing git repo..."
    git init .
    git add -A
    git commit -m "Initial commit"
}
